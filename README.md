# Introduction
Sometimes when I write Java code and need to throw an exception, I vaguely remember the exception’s name, but not to the exact spelling. For that, I am compiling this on-going list of exceptions to remind myself.


This project is simply a list of Java Exceptions. It started life as a blog [post](https://wuhrr.wordpress.com/2007/11/22/java-exceptions-list/).

# Usage

The usage is simple: just open the file *exceptions.txt* and skim through, or you can search the list for some familiar terms.
